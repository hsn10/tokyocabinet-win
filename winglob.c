#include "myconf.h"
#include "tcutil.h"

void globfree(glob_t *_pglob)
{
  size_t i;
  if (!_pglob->gl_pathv)
    return;
  for (i=0; i<_pglob->gl_pathc; i++)
    if (_pglob->gl_pathv[i])
      free(_pglob->gl_pathv[i]);
  free(_pglob->gl_pathv);
  _pglob->gl_offs = _pglob->gl_pathc = 0;
  _pglob->gl_pathv = NULL;
}

int glob(const char *pattern, int flags,
         int (*errfunc) (const char *epath, int eerrno),
         glob_t *pglob) {
	WIN32_FIND_DATAA stat;
	char *directory=NULL;
	int dirlen = 0;
	/* init pglob */
	pglob->gl_pathc = 0;
	pglob->gl_offs = 10;
	if ( (pglob->gl_pathv = calloc(10, sizeof(char *))) == NULL)
		return GLOB_NOSPACE;
	char *separator = strrchr(pattern,'/');

	if(separator!=NULL){
		directory = tccalloc(1, separator-pattern+2);
		strncpy(directory, pattern, separator-pattern+1);
		dirlen = strlen(directory);
	}
	HANDLE h = FindFirstFileA(pattern, &stat);
	if ( h == INVALID_HANDLE_VALUE ) {
		globfree(pglob);
		if(directory!=NULL)
			TCFREE(directory);
		return GLOB_ABORTED;
	}
	do {
		/* add new path to list */
		char *entry = tccalloc(1, strlen(stat.cFileName)+dirlen+1);
		if(directory!=NULL){
			strcpy(entry,directory);
		}
		strcat(entry, stat.cFileName);
		pglob->gl_pathv[pglob->gl_pathc++] = entry;
		if (pglob->gl_pathc == pglob->gl_offs) {
			/* increase array size */
			void *tmp = realloc(pglob->gl_pathv, sizeof(char *)*(pglob->gl_offs+10));
			if (tmp == NULL) {
				globfree(pglob);
				if(directory!=NULL)
					TCFREE(directory);
				return GLOB_NOSPACE;
			}
			pglob->gl_offs += 10;
			pglob->gl_pathv = tmp;
		}
	} while (FindNextFileA(h, &stat));
	FindClose(h);
	if(directory!=NULL)
		TCFREE(directory);
	return 0;
}
