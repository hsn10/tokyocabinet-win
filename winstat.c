#ifdef __MINGW32__
#include "myconf.h"
#undef fstat

static int file_attr_to_st_mode (DWORD attr)
{
	int fMode = S_IREAD;
	if (attr & FILE_ATTRIBUTE_DIRECTORY)
		fMode |= S_IFDIR;
	else
		fMode |= S_IFREG;
	if (!(attr & FILE_ATTRIBUTE_READONLY))
		fMode |= S_IWRITE;
	return fMode;
}

static long long filetime_to_hnsec(const FILETIME *ft)
{
	long long winTime = ((long long)ft->dwHighDateTime << 32) + ft->dwLowDateTime;
	/* Windows to Unix Epoch conversion */
	return winTime - 116444736000000000LL;
}

static time_t filetime_to_time_t(const FILETIME *ft)
{
	return (time_t)(filetime_to_hnsec(ft) / 10000000);
}

int winfstat(HANDLE fh, struct stat *buf)
{
        BY_HANDLE_FILE_INFORMATION fdata;

        if (fh == INVALID_HANDLE_VALUE) {
                errno = EBADF;
                return -1;
        }
        if (GetFileType(fh) != FILE_TYPE_DISK) {
                errno = ENODEV;
                return -1;
        }

        if (GetFileInformationByHandle(fh, &fdata)) {
                buf->st_ino = 0;
                buf->st_gid = 0;
                buf->st_uid = 0;
                buf->st_nlink = 1;
                buf->st_mode = file_attr_to_st_mode(fdata.dwFileAttributes);
                buf->st_size = fdata.nFileSizeLow |
                        (((off_t)fdata.nFileSizeHigh)<<32);
                buf->st_dev = buf->st_rdev = 0; /* not used by Git */
                buf->st_atime = filetime_to_time_t(&(fdata.ftLastAccessTime));
                buf->st_mtime = filetime_to_time_t(&(fdata.ftLastWriteTime));
                buf->st_ctime = filetime_to_time_t(&(fdata.ftCreationTime));
                return 0;
        }
        errno = EBADF;
        return -1;
}
#endif
