#include "myconf.h"
#include <sys/types.h>
#include <pthread.h>

int win_ftruncate(HANDLE fd, off_t length) {
  LARGE_INTEGER size;
  size.QuadPart = length;
  static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
  bool err = false;
  pthread_mutex_lock(&mutex);
  if (!SetFilePointerEx(fd, size, NULL, FILE_BEGIN)) err = true;
  if ( (err == false) && (!SetEndOfFile(fd)) ) err = true; 
  pthread_mutex_unlock(&mutex);
  /* && GetLastError() != 1224) return -1; */
  if (err) 
    return -1;
  return 0;
}
