#ifdef __MINGW32__

#include <time.h>
#include <string.h>
#include <pthread.h>

struct tm *gmtime_r(const time_t *timep, struct tm *result) {
  static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
  pthread_mutex_lock(&mutex);
  struct tm *cas = gmtime(timep);
  pthread_mutex_unlock(&mutex);
  if (result == NULL) {
    return NULL; /* was:cas */
  }
  memcpy(result, cas, sizeof(struct tm));
  return result;
}

struct tm *localtime_r(const time_t *timep, struct tm *result) {
  static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
  pthread_mutex_lock(&mutex);
  struct tm *cas = localtime(timep);
  pthread_mutex_unlock(&mutex);
  if (result == NULL) {
    return NULL;
  }
  memcpy(result, cas, sizeof(struct tm));
  return result;
}

#endif
